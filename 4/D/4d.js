function validate() {
    var name, surname, age, email, name_id, email_id, email_format_id, age_id;

	name_id = "Name is required";
    age_id = "Age is required";
    email_id = "Email is required";
    email_format_id = "Email has wrong format";
	
    name = document.getElementById("name").value;
    surname = document.getElementById("surname").value;
    age = document.getElementById("age").value;
    email = document.getElementById("email").value;

    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var is_valid = true;

    if (name === "") {
        document.getElementById("name_id").innerHTML = name_id;
        is_valid = false;
    } else {
        document.getElementById("name_id").innerHTML = "";
    }

    if (age === "") {
        document.getElementById("age_id").innerHTML = age_id;
        is_valid = false;
    } else {
        document.getElementById("age_id").innerHTML = "";
    }

    if (email === "") {
        document.getElementById("email_id").innerHTML = email_id;
        is_valid = false;
    } else if (!regex.test(email)) {
        document.getElementById("email_id").innerHTML = "";
        document.getElementById("email_format_id").innerHTML = email_format_id;
        is_valid = false;
    } else {
        document.getElementById("email_id").innerHTML = "";
        document.getElementById("email_format_id").innerHTML = "";
    }

    return is_valid;
}