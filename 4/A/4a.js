var FahrenheitsToCelsius = function(value) {
	return (value - 32) * 5 / 9;
}

var CelsiusToFahrenheits = function(value) {
	return value * 9 / 5 + 32;
}

function submit() {
    var valueCelcius = document.getElementById("valueCelcius").value;
    var valueFahrenheits = document.getElementById("valueFahrenheits").value;
    document.getElementById("celsius").textContent = FahrenheitsToCelsius(valueFahrenheits);
    document.getElementById("fahrenheit").textContent = CelsiusToFahrenheits(valueCelcius);
}