function amountTocoins(amount, coins)
{
    if (amount === 0)
        return [];

    if (coins[0] <= amount)
    {
        var left = (amount - coins[0]);
        return [coins[0]].concat(amountTocoins(left, coins));
    }
    else
    {
        coins.shift();
        return amountTocoins(amount, coins);
    }
}

console.log(amountTocoins(46, [23, 12, 1, 5, 2]));