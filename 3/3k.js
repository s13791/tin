function returnTypeOf(object)
{
    return typeof (object);
}

var movie = {
    lngth: 120,
    quality: '4K',
    type: 'sci-fi',
    changeLngth(newLngth) {
        this.ram = newLngth;
    },
    changeType(newType) {
        this.type = newType;
    }
};

function showMovie(Movie)
{
    console.log(Movie.lngth + ", lngth type: " + returnTypeOf(Movie.lngth));
    console.log(Movie.quality + ", quality type: " + returnTypeOf(Movie.quality));
    console.log(Movie.type + ", type type: " + returnTypeOf(Movie.type));
}

showMovie(movie);