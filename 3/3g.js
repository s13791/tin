function typeOf(object)
{
    return typeof (object);
}

var number = 2;
console.log(typeOf(number));

var string = "stringTest";
console.log(typeOf(string));