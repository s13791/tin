function searchHigestAndLowest(numbers)
{
    var max = Math.max.apply(null, numbers);
    numbers.splice(numbers.indexOf(max), 1);
    var finalMax = Math.max.apply(null, numbers);

    var min = Math.min.apply(null, numbers);
    numbers.splice(numbers.indexOf(min), 1);
    var finalMin = Math.min.apply(null, numbers);

    console.log("Lowest number " + finalMax);
    console.log("Highest number " + finalMin);
}

var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
searchHigestAndLowest(numbers);
