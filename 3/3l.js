function Student(firstname, surname, index, grades)
{
    this.firstname = firstname;
    this.surname = surname;
    this.index = index;
    this.grades = grades;

    this.average = function ()
    {
        var sum = 0;
        for (let i = 0; i < this.grades.length; i++)
        {
            sum += this.grades[i];
        }

        return sum / this.grades.length;
    };

    this.info = function () {
        console.log(`Name :${this.firstname}`);
        console.log(`Surname :${this.surname}`);
        console.log(`Average :${this.average()}`);
    };
}

var newStudent = new Student("Stefan", "Hula", "13264", [2, 4, 4, 5, 3]);
newStudent.info();
