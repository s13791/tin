function binarySearch(array, target)
{
    let left = 0;
    let right = array.length - 1;

    while (left <= right)
    {
        const mid = left + Math.floor((right - left) / 2);
        if (array[mid] === target)
        {
            return mid;
        }

        if (array[mid] < target)
        {
            left = mid + 1;
        } else
        {
            right = mid - 1;
        }
    }
    return -1;
}

const toCheck = [3, 5, 89, 66, 11, 74, 8, 199];
console.log(binarySearch(toCheck, 89));