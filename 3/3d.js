function sortAlphabetically(word)
{
    var arrayString = word.split('');
    var sortedString = arrayString.sort();

    return sortedString.join('');
}

var check = 'firstTest';
console.log(sortAlphabetically(check));
var check = 'secondTest';
console.log(sortAlphabetically(check));