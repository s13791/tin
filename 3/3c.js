function palindrome(word)
{
	var regex = /[\W_]/g;
  	var lowRegStr = word.toLowerCase().replace(regex, '');
  	var reversedString = lowRegStr.split('').reverse().join(''); 
  	return reversedString === lowRegStr;
}

console.log(palindrome("Kajak"));
console.log(palindrome("Niekajak"));