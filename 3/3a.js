function facIter(n)
{
    if (n < 0)
    {
        return "Wrong number";
    }
    else
    {
        if (n === 1 || n === 0)
        {
            return 1;
        }
        else
        {
            var result = 1;
            while (n > 0)
            {
                result = result * n;
                n--;
            }
            return result;
        }
    }
}

var number = 4;
console.log(facIter(number));

var fact = function facRec(input) { return input < 2 ? 1 : input * facRec(input - 1); };
console.log(fact(number));