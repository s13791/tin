import React, { Component } from 'react';
import './App.css';
import Main from './modules/Main';
import Header from './modules/Header';
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Header />
          <Main />
        </header>
      </div>
    );
  }
}

export default App;
