import React from 'react';
import PropTypes from 'prop-types';

const Values = ({
    nameVal,
    yobVal,
    emailVal,
}) => {
    return (
        <div className="body">
            <h2>Values</h2>
            <div className="values">
                <p>Name: {nameVal === undefined ? 'empty' : nameVal}</p>
                <p>Year of Birth: {yobVal === undefined ? 'empty' : yobVal}</p>
                <p>email address: {emailVal === undefined ? 'empty' : emailVal}</p>
            </div>
        </div>
    )
};

Values.propTypes = {
    nameVal: PropTypes.string,
    nameVal: PropTypes.number,
    emailVal: PropTypes.string,
};

export default Values;
