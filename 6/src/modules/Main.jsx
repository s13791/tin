import React from 'react';
import { Switch, Route } from 'react-router-dom';
import FormComp from './FormComp';
import Values from './Values';
import Message from './Message';

function Main() {
  return (
    <main>
      <Switch>
        <Route exact path='/' component={FormComp}/>
        <Route path='/values' component={Values}/>
        <Route path='/message' component={Message}/>
      </Switch>
    </main>
  );
}

export default Main;
