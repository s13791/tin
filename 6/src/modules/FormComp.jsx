import React from 'react';
// import PropTypes from 'prop-types';
import Message from './Message';
import Values from './Values';

class FormComp extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            nameVal: '',
            yobVal: '',
            emailVal: '',
            messages: [],
            renderComp: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.validateClick = this.validateClick.bind(this);
    }

    // state = {
    //     nameVal: '',
    //     yobVal: '',
    //     emailVal: '',
    //     messages: [],
    // };

    handleChange = (event) => {
        this.setState(
            {
                [event.target.name] : event.target.value
            }
        );
    }

    validateClick = () => {
        console.log('validateClick clicked!');
        let newTab = [];
        if (this.state.nameVal === '' && this.state.yobVal === '' && this.state.emailVal === '') {
            newTab.push('Nothing to validate.');
            this.setState(
                {
                    messages : newTab,
                    renderComp : true,
                }
            ); 
        } else {
            newTab = [];
            if (this.state.nameVal) {
                if (this.state.nameVal.length < 4) {
                    newTab.push('Name must have at least 4 letters');
                    this.setState(
                        {
                            messages : newTab,
                            renderComp : true,
                        }
                    ); 
                }
            }
            if (this.state.yobVal) {
                if (this.state.yobVal.length !== 4 ) {
                    newTab.push('Year of birth must have 4 numbers');
                }
                if (this.state.yobVal <= 1900 ) {
                    newTab.push('Year of birth must higher than 1900');
                }
                let currentYear = new Date().getFullYear();
                if (this.state.yobVal > currentYear ) {
                    newTab.push('Year of birth can not be higher than current year');
                }; 
            }
            if (this.state.emailVal) {
                if (this.state.emailVal.indexOf("@") === -1) {
                    newTab.push('email address must contain "@" character');
                }
                if (this.state.emailVal.indexOf(".") === -1) {
                    newTab.push('email address must contain "." character');
                }
            }
            if (newTab.length === 0) {
                newTab.push('values are valid');
            }
            this.setState(
                {
                    messages : newTab,
                    renderComp : true,
                }
            );
        }

    }

    render() {

        return (
            <div>
                <div className="topDisplay">
                    <form onSubmit={()=> {console.log('submitting...')}} className="body">
                        <h2>Form</h2>
                        <div className="formList">
                            <div className="formItem">
                                <span>Name: </span>
                                <input
                                    type="text"
                                    id="name"
                                    name="nameVal"
                                    required
                                    minlength="4"
                                    maxlength="15"
                                    size="20"
                                    value={this.state.nameVal}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="formItem">
                                <span>Year of Birth: </span>
                                <input
                                    type="number"
                                    id="name"
                                    name="yobVal"
                                    required
                                    minlength="4"
                                    maxlength="4"
                                    value={this.state.yobVal}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="formItem">
                                <span>Email address: </span>
                                <input
                                    type="email"
                                    id="name"
                                    name="emailVal"
                                    required
                                    minlength="4"
                                    maxlength="15"
                                    size="20"
                                    value={this.state.emailVal}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="buttons">   
                            <input
                                className="submitButton"
                                type="button"
                                value="Validate"
                                onClick={() => this.validateClick()}
                            />
                        </div>
                    </form>
                    <Values 
                        nameVal={this.state.nameVal}
                        yobVal={this.state.yobVal}
                        emailVal={this.state.emailVal}
                    />
                </div>
                {this.state.renderComp ? <Message 
                        message={this.state.messages}
                    /> : null 
                }
            </div>
        );
    }
}

export default FormComp;