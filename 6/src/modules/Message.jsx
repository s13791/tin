import React from 'react';
import PropTypes from 'prop-types';

const Message = ({message}) => {
    const displayMessage = (messages) => {
        if (messages) {
            if(messages.length > 0) {
                return messages.map((item, key) => {
                    console.log(key + item);
                    return (
                        <div key={key}>
                            <p>{item}</p>
                        </div>
                    );
                })
            }
        } else {
            return (<p>No messages.</p>);
        }
    };

    return (
        <div className="body">
            <h2>Validation messages: </h2>
            {displayMessage(message)}
        </div>
    )
};

Message.propTypes = {
    message: PropTypes.array,
};

export default Message;
