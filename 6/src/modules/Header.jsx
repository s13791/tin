import React from 'react';
import { Link } from 'react-router-dom'

function Header() {
  return (
    <header className="body blackBG">
      <nav>
        <span>Navigation through components using react router: </span>
        <div className="navList">
          <span><Link to='/'>Form</Link></span>
          <span><Link to='/values'>Values</Link></span>
          <span><Link to='/message'>Message</Link></span>
        </div>
      </nav>
    </header>
  );
}

export default Header