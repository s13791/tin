const events = require("events");
var http = require('http');

var server = http.createServer(function(
    request, response) {
    routeAddress(request,
        response);
});

const myEmitter = new events.EventEmitter();

myEmitter.on('error', err => {
    console.log(
        'error: ${err.message}'
    );
});

function routeAddress(request, response) {
    [_, op, a, b] = request.url.split(
        "/");
		
    var op = op;
    var a = parseInt(a);
    var b = parseInt(b);
	var isValid = true;
	
    response.writeHead(200, {
        "Content-Type": "text/html"
    });
	
	if (op == "sub") {
        var responseMsg = a + "-" +
            b + "=" + (a - b);
        isValid = false;
    } else if (op == "add") {
        var responseMsg = a + "+" +
            b + "=" + (a + b);
        isValid = false;
    } else if (op == "div") {
        var responseMsg = a + "/" +
            b + "=" + (a / b);
        isValid = false;
    } else if (op == "mul") {
        var responseMsg = a + "*" +
            b + "=" + (a * b);
        isValid = false;
    } else if (op != "add" || op !=
        "sub" || op != "div" || op !=
        "mul") {
        var responseMsg =
            "Wrong operation input";
    } else if (!isNaN(a) || isNaN(b)) {
        var responseMsg =
            "Wrong parameters format";
    }

    response.end(responseMsg)
}

server.listen(8090, "127.0.0.1");
