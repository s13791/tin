const express = require('express')
const app = express()

app.set('view engine', 'pug')

const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.get('/formdata', function(request, response){ 
    response.render('formdata', {title: 'Form data', header: 'Form data',firstname: "First name: " + request.query.firstname,
        lastname: "Last name: " + request.query.lastname, email: "Email: " + request.query.email});
});
app.get('/jsondata', function(request, response){
    response.render('jsondata', {title: 'Json Data', header:'JSON data', firstname: "First name: " + request.body.firstname, 
        lastname: 'Last name: ' + request.body.lastname,  email: 'Email: ' + request.body.email} )
});
app.get('/hello', (request, response) => response.send('Hello World!'));
app.get('/form', (request, response) => response.sendFile(__dirname+ '/index.html'));

app.listen(8080, '127.0.0.1')

