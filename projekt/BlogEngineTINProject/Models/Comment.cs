﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlogEngineTINProject.Models
{
    public class Comment
    {
        [Required]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        [Required]
        public string Author { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public DateTime PubDate { get; set; } = DateTime.UtcNow;

        public bool IsAdmin { get; set; }

        public string RenderContent()
        {
            return Content;
        }
    }
}
