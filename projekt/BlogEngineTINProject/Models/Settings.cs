﻿namespace BlogEngineTINProject.Models
{
    public class Settings
    {
        public string Owner { get; set; } = "The Owner";
        public int CommentsCloseAfterDays { get; set; } = 10;
        public int PostsPerPage { get; set; } = 4;
    }
}
