﻿namespace BlogEngineTINProject.Services.Interfaces
{
    public interface IUserServices
    {
        bool ValidateUser(string username, string password);
    }
}
