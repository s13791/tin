﻿using System;
using BlogEngineTINProject.Services.Interfaces;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;

namespace BlogEngineTINProject.Services
{
    public class BlogUserServices: IUserServices
    {
        private readonly IConfiguration _config;

        public BlogUserServices(IConfiguration config)
        {
            _config = config;
        }

        public bool ValidateUser(string username, string password)
        {
            return CheckPassword(password, _config) && username == _config["user:username"];
        }

        private static bool CheckPassword(string password, IConfiguration config)
        {
            var hashBytes = KeyDerivation.Pbkdf2(
                password,
                new byte[0], 
                KeyDerivationPrf.HMACSHA1,
                1000,
                256 / 8
            );

            var hashText = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
            return hashText == config["user:password"];
        }
    }
}
